package id.ac.ui.cs.advprog.tutorial8.adapter.service;

import id.ac.ui.cs.advprog.tutorial8.adapter.core.*;

import java.util.ArrayList;
import java.util.List;

public class MagicService {
    List<Magic> allMagic =  new ArrayList<>();

    public MagicService() {
        allMagic.add(new FireMagic());
        allMagic.add(new SpiritMagic());
        allMagic.add(new SwordMagic());
        //@TODO Add the Mage Aspect and Lost Attribute to this Lisr
    }

    public List<MagicResponseWrapper> findAll() {
        List<MagicResponseWrapper> responseWrappers = new ArrayList<>();
        for (Magic magic : allMagic) {
            responseWrappers.add(convertToWrapper(magic));
        }
        return responseWrappers;
    }

    public MagicResponseWrapper convertToWrapper(Magic magic) {
        MagicResponseWrapper responseWrapper = new MagicResponseWrapper();
        responseWrapper.setCast(magic.cast());
        responseWrapper.setMagicName(magic.description());
        return responseWrapper;
    }

    public Magic getMagicInstance(String magicName) {
        if (magicName.equalsIgnoreCase("Fire Magic")) {
            return new FireMagic();
        } else if (magicName.equalsIgnoreCase("Spirit Magic")) {
            return new SpiritMagic();
        } // @TODO add Adapter Magic Instance to condition for this
        else {
            return new SwordMagic();
        }
    }


}
