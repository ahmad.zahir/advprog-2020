package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class FireMagic implements Magic {

    @Override
    public String cast() {
        return "LALALALALA, i mean fire bird, i mean fire magic";
    }

    @Override
    public int getMagicCost() {
        return 50;
    }

    @Override
    public String description() {
        return "Fire Magic";
    }
}
