package id.ac.ui.cs.tutorial9.factory.core.factory;

import id.ac.ui.cs.tutorial9.factory.core.parts.*;

// TO DO : Complete the implementation of this factory
// Affinity : Might
// Aura : CrimsonDemonDominance
// Spell : Explosion
// Weapon : Staff
public class WarlockFactory implements KnightFactory {
    public Affinity createAffinity(){
        return new Might();
    }

    public Aura createAura(){
        return new CrimsonDemonDominance();
    }

    public Spell createSpell(){
        return new Explosion();
    }

    public Weapon createWeapon(){
        return new Staff();
    }
}