package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Justice implements Affinity {
    public String getDescription(){
        return "Justice : This knight acts using their conscience";
    }
}