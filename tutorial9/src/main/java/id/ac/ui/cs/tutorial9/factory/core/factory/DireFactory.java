package id.ac.ui.cs.tutorial9.factory.core.factory;

import id.ac.ui.cs.tutorial9.factory.core.parts.*;

// TO DO : Complete the implementation of this factory
// Affinity : ThunderCrossSplitAttack
// Aura : ThunderCrossSplitAttack
// Spell : ThunderCrossSplitAttack
// Weapon : ThunderCrossSplitAttack
public class DireFactory implements KnightFactory {
    public Affinity createAffinity(){
        return new ThunderCrossSplitAttack();
    }

    public Aura createAura(){
        return new ThunderCrossSplitAttack();
    }

    public Spell createSpell(){
        return new ThunderCrossSplitAttack();
    }

    public Weapon createWeapon(){
        return new ThunderCrossSplitAttack();
    }
}