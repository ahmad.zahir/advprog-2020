package id.ac.ui.cs.tutorial9.factory.core.factory;

import id.ac.ui.cs.tutorial9.factory.core.parts.*;

// TO DO : Complete the implementation of this factory
// Affinity : Justice
// Aura : SelfOfferingSacrifice
// Spell : StoutGuardian
// Weapon : Shield
public class PaladinFactory implements KnightFactory {
    public Affinity createAffinity(){
        return new Justice();
    }

    public Aura createAura(){
        return new SelfOfferingSacrifice();
    }

    public Spell createSpell(){
        return new StoutGuardian();
    }

    public Weapon createWeapon(){
        return new Shield();
    }
}