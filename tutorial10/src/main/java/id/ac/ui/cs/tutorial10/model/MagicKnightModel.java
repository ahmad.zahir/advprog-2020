package id.ac.ui.cs.tutorial10.model;

import id.ac.ui.cs.tutorial10.enumattr.Position;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "MagicKnights")
@Getter @Setter @NoArgsConstructor
public class MagicKnightModel implements Serializable {

    @Id
    @Column(name = "name")
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "magic_id", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MagicModel magic;

    @NotNull
    @Column(name = "position")
    @Enumerated(EnumType.STRING)
    private Position position;
}