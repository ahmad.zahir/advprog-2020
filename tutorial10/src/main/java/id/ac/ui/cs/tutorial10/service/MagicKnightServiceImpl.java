package id.ac.ui.cs.tutorial10.service;

import id.ac.ui.cs.tutorial10.model.MagicKnightModel;
import id.ac.ui.cs.tutorial10.repository.MagicKnightRepository;
import reactor.core.publisher.Flux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MagicKnightServiceImpl implements MagicKnightService {

    @Autowired
    private MagicKnightRepository repo;

    @Override
    public Flux<MagicKnightModel> findAll() {
        //ToDo: Implement this. Make sure to return a Flux<MagicKnightModel> reactive stream.
        return null;
    }

    public MagicKnightModel addKnight(MagicKnightModel knight){
        //ToDo: Implement this.
        return null;
    }
}